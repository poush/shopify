# Copyright 2015 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.documentation import include_docs_urls
from graphene_django.views import GraphQLView

from shopify import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'shops', views.ShopViewSet)
router.register(r'orders', views.OrderViewSet)
router.register(r'products', views.ProductViewSet)
router.register(r'lineitems', views.LineitemViewSet)



urlpatterns = [
	url(r'^docs/', include_docs_urls(title='My API title')),
    path('admin/', admin.site.urls),
    url(r'^graphql', GraphQLView.as_view(graphiql=True)),
	url(r'^', include(router.urls)),
]

# Only serve static files from Django during development
# Use Google Cloud Storage or an alternative CDN for production
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
