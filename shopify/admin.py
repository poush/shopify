from django.contrib import admin
from .models import Shop, Product, Order, Lineitem

admin.site.register(Shop)
admin.site.register(Product)
admin.site.register(Order)
admin.site.register(Lineitem)