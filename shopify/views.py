from django.contrib.auth.models import User
from rest_framework import viewsets

from .serializers import UserSerializer, ShopSerializer, ProductSerializer, LineitemSerializer, OrderSerializer
from .models import Shop, Product, Order, Lineitem


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class ShopViewSet(viewsets.ModelViewSet):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.filter(active=True)
    serializer_class = ProductSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class LineitemViewSet(viewsets.ModelViewSet):
    queryset = Lineitem.objects.all()
    serializer_class = LineitemSerializer