from django.forms import ModelForm
from .models import Shop, Order, Product, Lineitem

class ShopForm(ModelForm):
	class Meta:
		model = Shop
		fields = ['name', 'slug']