from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Shop, Order, Product, Lineitem


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')


class ShopSerializer(serializers.HyperlinkedModelSerializer):
    # owner = serializers.HyperlinkedIdentityField(view_name='user-detail', lookup_field="owner")
    # highlight = serializers.HyperlinkedIdentityField(view_name='users', format='html')
    
    class Meta:
        model = Shop
        fields = ('id', 'name', 'slug', 'owner', 'products')


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    # Lineitems = serializers.HyperlinkedIdentityField(view_name='Lineitem-list')

    class Meta:
        model = Order
        fields = ('id', 'total', 'totalQty', 'lineitems')


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    # shop = serializers.HyperlinkedIdentityField(view_name='shop-detail')

    class Meta:
        model = Product
        fields = ('id', 'qty', 'name', 'price', 'active', 'shop', 'lineitems')


class LineitemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lineitem
        fields = ('id', 'qty', 'product', 'order')
