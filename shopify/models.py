from django.db import models

# Create your models here.
class Shop(models.Model):
	name = models.CharField(max_length=100)
	slug = models.CharField(max_length=50)
	owner =  models.ForeignKey('auth.User', on_delete=models.CASCADE)

	def __str__(self):
		return self.name


class Product(models.Model):
	name = models.CharField(max_length=100)
	qty = models.PositiveIntegerField()
	price = models.DecimalField(max_digits=8, decimal_places=2)
	shop = models.ForeignKey(Shop, related_name='products', on_delete=models.CASCADE)
	active = models.BooleanField(default=False)
	
	def __str__(self):
		return self.name + '_' + self.shop.name


class Order(models.Model):
	total = models.DecimalField(max_digits=10, decimal_places=2)
	totalQty = models.PositiveIntegerField()
	shop = models.ForeignKey(Shop, related_name='orders', on_delete=models.CASCADE)
	ordered_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)



class Lineitem(models.Model):
	product = models.ForeignKey(Product,related_name='lineitems', on_delete=models.CASCADE)
	order = models.ForeignKey(Order, related_name='lineitems', on_delete=models.CASCADE)
	qty = models.PositiveIntegerField()