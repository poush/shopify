from graphene import relay, ObjectType
from graphene_django import DjangoObjectType
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.filter import DjangoFilterConnectionField

from .models import Shop, Order, Product, Lineitem
from .forms import ShopForm


class ShopNode(DjangoObjectType):
    class Meta:
        model = Shop
        filter_fields = ['id', 'name']
        exclude_fields = ('owner')
        interfaces = (relay.Node, )


class ShopMutation(DjangoModelFormMutation):
    class Meta:
        form_class = ShopForm

class ProductNode(DjangoObjectType):
    class Meta:
        model = Product
        filter_fields = ['id', 'name']
        interfaces = (relay.Node, )

    @classmethod
    def get_node(cls, id, info):
        try:
            prod = cls._meta.model.objects.get(id=id)
        except cls._meta.model.DoesNotExist:
            return None

        if prod.active or info.context.user == prod.shop.owner:
            return post
        return None


class OrderNode(DjangoObjectType):
    class Meta:
        model = Order
        filter_fields = []
        exclude_fields = ('ordered_by')
        interfaces = (relay.Node, )


class LineitemNode(DjangoObjectType):
    class Meta:
        model = Lineitem
        filter_fields = []
        interfaces = (relay.Node, )


class Query(object):

    shop = relay.Node.Field(ShopNode)
    order = relay.Node.Field(OrderNode)
    product = relay.Node.Field(ProductNode)
    lineitem = relay.Node.Field(LineitemNode)
    all_products = DjangoFilterConnectionField(ProductNode)
    all_shops = DjangoFilterConnectionField(ShopNode)
    all_orders = DjangoFilterConnectionField(OrderNode)
    all_lineItems = DjangoFilterConnectionField(LineitemNode)


    def resolve_all_products(self, info, **kwargs):
        return Product.objects.filter(active=True)

    # def resolve_all_shops(self, info, **kwargs):
    #     # We can easily optimize query count in the resolve method
    #     return Shop.objects.all()

    # def resolve_all_orders(self, info, **kwargs):
    #     return Order.objects.all()

    # def resolve_order(self, info, **kwargs):
    #     id = kwargs.get('id')

    #     if id is not None:
    #         return Order.objects.get(pk=id)

    #     return None

    # def resolve_shop(self, info, **kwargs):
    #     id = kwargs.get('id')

    #     if id is not None:
    #         return Shop.objects.get(pk=id)

    #     return None

    # def resolve_product(self, info, **kwargs):
    #     id = kwargs.get('id')

    #     if id is not None:
    #         return Product.objects.get(pk=id)

    #     return None

class Mutation(object):
    create_shop = ShopMutation.Field()